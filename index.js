const express = require('express');
const app = express();
// Set up aws account, create S3 bucket and upload images
const aws = require("aws-sdk");
// loading config file with accesskey, secretkey and region
aws.config.loadFromPath('./config.json');

app.use(express.static('public'));

// Creating new instance of Rekognition
const rekognition = new aws.Rekognition();
const bucket        = 'facesimages'; // the bucketname without s3://
const photo_source  = 'got-hair.jpg';
const photo_target  = 'no-hair.jpg';

// These are the parameters to pass 
const comparisonParams = {
    SourceImage: {
        S3Object: {
        Bucket: bucket,
        Name: photo_source
        },
    },
    TargetImage: {
        S3Object: {
        Bucket: bucket,
        Name: photo_target
        },
    },
    SimilarityThreshold: 70
}

app.get('/api/facesdemo', function(req, res, next) {
    // Running compareFaces (does all the heavy lifting) and getting our data
    rekognition.compareFaces(comparisonParams, function(err, response) {
        if (err) {
            console.log(err, err.stack);
        } else {
            response.FaceMatches.forEach(data => {
                res.json(data);
            });
        }
    });
});

app.listen(5555, () => {
    console.log('Listening on port 5555!');
})